#include<iostream>
#include<vector>

using namespace std;

class Engine
{
    private:
        string str1;
        string str2;
        vector<int> mainAlpha;
    
        vector<int> initializeVector(vector<int> vec , int size)
        {
            for(int i = 0 ; i < size ; i++)
            {
                vec.push_back(0);
            }
            return vec;
        }
    
    public:
        Engine(string s1 , string s2)
        {
            str1      = s1;
            str2      = s2;
            mainAlpha = initializeVector(mainAlpha , 26);
        }
    
        vector<int> findUncommonCharacters()
        {
            vector<int> alpha1;
            alpha1  = initializeVector(alpha1 , 26);
            vector<int> alpha2;
            alpha2  = initializeVector(alpha2 , 26);
            int len = (int)str1.length();
            for(int i = 0 ; i < len ; i++)
            {
                alpha1[str1[i]-'a'] = 1;
            }
            len = (int)str2.length();
            for(int i = 0 ; i < len ; i++)
            {
                alpha2[str2[i]-'a'] = 1;
            }
            for(int i = 0 ; i < 26 ; i++)
            {
                mainAlpha[i] = alpha1[i] + alpha2[i];
            }
            return mainAlpha;
        }
    
        void displayUncommonCharaFromList(vector<int> alphaVector)
        {
            int size = (int)alphaVector.size();
            for(int i = 0 ; i < size ; i++)
            {
                if(alphaVector[i] == 1)
                {
                    cout<<(char)('a'+i)<<" ";
                }
            }
            cout<<endl;
        }
};

int main()
{
    string str1 = "geeksforgeeks";
    string str2 = "geeksquiz";
    Engine e    = Engine(str1 , str2);
    vector<int> uncommonCharactersVector = e.findUncommonCharacters();
    e.displayUncommonCharaFromList(uncommonCharactersVector);
    return 0;
}
